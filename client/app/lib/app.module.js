'use strict';

var app = angular.module('fonflatter', [

  /*
   * Angular modules
   */
  'angular-meteor',
  'ngAnimate',
  'ngMaterial',
  'ngRoute',

  /*
   * 3rd Party modules
   */
  'pascalprecht.translate'
]);

function onReady() {
  angular.bootstrap(document, ['fonflatter']);
}

angular.element(document).ready(onReady);
