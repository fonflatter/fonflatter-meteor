Comments = new Mongo.Collection('comments', {idGeneration: 'MONGO'});

if (Meteor.isServer) {
  Comments._ensureIndex({
    pageId: 'hashed'
  });
}
