'use strict';

var app = angular.module('fonflatter');

app.config(configureI18N);

configureI18N.$inject = ['$translateProvider'];

function configureI18N($translateProvider) {

  $translateProvider.translations('en', {
    TITLE: 'fred<small>the</small>bat.com',
    URL: 'http://fonflatter.com',
    ARCHIVE: {
      MONTH_NAMES: [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
        'Fredventskalender'
      ]
    },
    COMMENT_HOMEPAGE: 'Homepage',
    COMMENT_NAME: 'Name',
    COMMENT_SUBMIT: 'Submit',
    COMMENT_TEXT: 'Comment',
    MENU: 'Menu',
    MENU_ARCHIVE: 'Archive',
    MENU_CLOSE: 'Close',
    MENU_COMICS: 'Comics',
    MENU_COMMENTS: 'Comments',
    MENU_IMPRINT: 'Imprint',
    MENU_SEARCH: 'Search',
    MENU_SHOP: 'Fredshop',
    SEARCH: {
      MORE_RESULTS: 'More Results'
    },
    TODAY_IS: 'Today is <strong>Day of busy Fredmin</strong>.',
    TRANSCRIPTION_TEXT: 'Transcription'
  });

  $translateProvider.translations('de', {
    TITLE: 'fonflatter.de',
    URL: 'http://fonflatter.de',
    ARCHIVE: {
      MONTH_NAMES: [
        'Januar',
        'Februar',
        'März',
        'April',
        'Mai',
        'Juni',
        'Juli',
        'August',
        'September',
        'Oktober',
        'November',
        'Dezember',
        'Fredventskalender'
      ]
    },
    COMMENT_HOMEPAGE: 'Homepage',
    COMMENT_NAME: 'Name',
    COMMENT_SUBMIT: 'Abschicken',
    COMMENT_TEXT: 'Kommentar',
    MENU: 'Menü',
    MENU_ARCHIVE: 'Archiv',
    MENU_CLOSE: 'Schließen',
    MENU_COMICS: 'Comics',
    MENU_COMMENTS: 'Kommentare',
    MENU_IMPRINT: 'Impressum',
    MENU_SEARCH: 'Suchen',
    MENU_SHOP: 'Fredshop',
    SEARCH: {
      MORE_RESULTS: 'Mehr Ergebnisse'
    },
    TODAY_IS: 'Heute ist <strong>Tag des nicht implementierten Fredkalenders.</strong>',
    TRANSCRIPTION_TEXT: 'Transkription'
  });

  var currentHost = hosts[location.host];
  var language;

  if (currentHost) {
    language = currentHost.language;
  } else {
    language = 'en';
  }

  $translateProvider.preferredLanguage(language);
  $translateProvider.useSanitizeValueStrategy('sanitizeParameters');

}
