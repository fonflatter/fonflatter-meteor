'use strict';

var app = angular.module('fonflatter');

app.config(configureRoutes);

configureRoutes.$inject = ['$routeProvider'];

function configureRoutes($routeProvider) {

  $routeProvider
    .when('/archive/', {
      templateUrl: 'client/archive/archive.ng.html',
      controller: 'ArchiveController',
      controllerAs: 'archive'
    });

}
