'use strict';

var app = angular.module('fonflatter');

app.config(configureStates);

configureStates.$inject = ['$routeProvider'];

function configureStates($routeProvider) {
  $routeProvider
    .when('/404', {
      templateUrl: 'client/errors/404.ng.html',
      controller: ['page',
        function(page) {
          page.current = {
            title: 'Fehler'
          };
        }
      ]
    });

}
