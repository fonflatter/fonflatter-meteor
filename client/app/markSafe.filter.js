'use strict';

var app = angular.module('fonflatter');
app.filter('markSafe', ['$sce',
  function($sce) {
    return $sce.trustAsHtml;
  }
]);
