hosts = {
  'neu.fonflatter.de': {
    language: 'de'
  },
  'new.fredthebat.com': {
    language: 'en'
  }
};

var developmentPostfix = '.node.js:3000';
var isDevelopmentEnvironment;

if (Meteor.isServer) {
  isDevelopmentEnvironment = (process.env.NODE_ENV === 'development');
} else {
  isDevelopmentEnvironment = _.endsWith(location.host, developmentPostfix);
}

if (isDevelopmentEnvironment) {
  var productionHosts = hosts;
  hosts = {};

  _.forEach(productionHosts, function(config, productionHostname) {
    hosts[productionHostname + developmentPostfix] = config;
  });
}
