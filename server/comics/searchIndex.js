Meteor.startup(function() {

  Pages.searchIndex = lunr(function() {
    this.field('text');
    this.ref('searchRef');
  });

  Transcriptions.find({}, {
    fields: {
      pageId: 1,
      text: 1
    }
  })
    .forEach(function(transcription) {
      transcription.searchRef = transcription.pageId._str;
      Pages.searchIndex.add(transcription);
    });

});
