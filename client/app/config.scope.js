'use strict';

var app = angular.module('fonflatter');
app.run(configureScope);

configureScope.$inject = [
  '$rootScope',
  'menu',
  'page',
  'sidebar',
  'transcriptions'
];

function configureScope($rootScope, menu, page, sidebar,
                        transcriptions) {
  $rootScope.menu = menu;
  $rootScope.page = page;
  $rootScope.sidebar = sidebar;
  $rootScope.transcriptions = transcriptions;

  $rootScope.roundButton = 'md-primary md-raised md-fab';
}
