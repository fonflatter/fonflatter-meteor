'use strict';

var app = angular.module('fonflatter');
app.factory('search', searchFactory);

searchFactory.$inject = [
  '$meteor'
];

function searchFactory($meteor) {
  var search = {
    clear: clear,
    hasInput: hasInput,
    hasMorePages: hasMorePages,
    isEnabled: true,
    keywords: '',
    loadMorePages: loadMorePages,
    reset: reset,
    result: {},
    run: run
  };

  return search;

  function clear() {
    search.keywords = '';
    search.isEnabled = true;
    reset();
  }

  function hasInput() {
    return !_.isEmpty(search.keywords);
  }

  function hasMorePages() {
    if (!search.result.partitions) {
      return false;
    }

    var numPartitions = search.result.partitions.length;
    return search.result.partitionIndex < numPartitions - 1;
  }

  function loadMorePages() {
    ++search.result.partitionIndex;
    var partition = search.result.partitions[search.result.partitionIndex];

    return $meteor.call('loadSearchResults', partition)
      .then(function(pages) {
        search.result.pages = search.result.pages.concat(pages);
      });
  }

  function reset() {
    search.result = {};
  }

  function run(searchInput) {
    if (!searchInput) {
      if (hasInput()) {
        searchInput = {
          keywords: search.keywords
        };
      } else {
        reset();
        return;
      }
    }

    return $meteor.call('search', searchInput)
      .then(function(result) {
        search.result = result;
      });
  }
}
