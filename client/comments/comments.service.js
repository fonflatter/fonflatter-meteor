'use strict';

var app = angular.module('fonflatter');
app.factory('comments', commentsFactory);

commentsFactory.$inject = [
  '$meteor'
];

function commentsFactory($meteor) {

  var comments = {
    load: loadComments
  };

  return comments;

  function loadComments(page) {
    return $meteor.subscribe('comments', page._id)
      .then(function(subscriptionHandle) {
        if (comments.subscriptionHandle) {
          comments.subscriptionHandle.stop();
        }

        comments.subscriptionHandle = subscriptionHandle;
        return page;
      });

  }
}
