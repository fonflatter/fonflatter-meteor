'use strict';

var app = angular.module('fonflatter');

app.controller('ComicController', ComicController);

ComicController.$inject = [
  '$http',
  '$location',
  '$meteor',
  '$route',
  '$translate',
  'comments',
  'page',
  'transcriptions'
];

function ComicController($http, $location, $meteor, $route, $translate,
                         comments, page, transcriptions) {

  page.current = undefined;

  var language = $translate.use();

  loadComic()
    .then(function(comic) {
      if (!comic) {
        return $location.url('/404');
      }

      return $http.get(comic.image)
        .then(function() {
          return comic;
        })
    })
    .then(function(comic) {
      comic.date = moment.utc(comic.date);
      page.current = comic;

      page.hasComments = true;

      return comic;
    })
    .then(comments.load)
    .then(transcriptions.load);

  function loadComic() {
    var routeParams = $route.current.params;

    if (routeParams.year && routeParams.month && routeParams.day) {
      var comicDate = moment.utc({
        year: routeParams.year,
        month: parseInt(routeParams.month) - 1, // month is 0-based
        day: routeParams.day
      });

      if (!comicDate.isValid()) {
        return $location.url('/404');
      }
      return $meteor.call('findComic', language, comicDate.toDate())

    } else {
      return $meteor.call('findMostRecentComic', language)
    }
  }

}
