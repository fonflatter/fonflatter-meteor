'use strict';

var app = angular.module('fonflatter');
app.factory('page', pageFactory);

pageFactory.$inject = [
  '$meteor',
  '$mdToast'
];

function pageFactory($meteor, $mdToast) {
  var page = {
    comment: {
      submit: addComment
    },
    current: undefined,
    hasComments: false,
    showUnimplementedToast: showUnimplementedToast
  };

  var unimplementedToast = $mdToast.simple()
    .position('top right')
    .content('Das geht noch nicht!')
    .hideDelay(2000);

  return page;

  function addComment() {
    $meteor.call('addComment', page.current._id, page.comment);
  }

  function showUnimplementedToast() {
    return $mdToast.show(unimplementedToast);
  }
}
