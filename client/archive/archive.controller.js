'use strict';

var app = angular.module('fonflatter');

app.controller('ArchiveController', ArchiveController);

ArchiveController.$inject = [
  '$translate',
  'page',
  'search',
  'sidebar'
];

function ArchiveController($translate, page, search, sidebar) {
  page.current = {
    title: 'Archiv'
  };

  var now = moment();
  var minYear = 2005;
  var maxYear = now.year();

  var viewModel = this;

  viewModel.months = {};
  viewModel.selectMonth = selectMonth;
  viewModel.selectedYearIndex = maxYear;
  viewModel.years = _.range(minYear, maxYear + 1);

  _.forEach(viewModel.years, function(year) {
    viewModel.months[year] = getMonthsForYear(year);
  });

  function getMonthsForYear(year) {
    var minMonth;

    if (year === minYear) {
      minMonth = 8; // Fred was born in September
    } else {
      minMonth = 0;
    }


    var maxMonth = 13; // add one for Advent calendar

    // TODO: admin can always see every month
    if ((year == now.year()) && (now.month() < 11)) {
      maxMonth = now.month() + 1;
    } else if (year < 2008) {
      maxMonth = 12; // no Advent calendar back in those days
    }

    return _.range(minMonth, maxMonth);
  }

  function selectMonth(month) {
    var year = viewModel.years[viewModel.selectedYearIndex];

    sidebar.search.isEnabled = false;

    $translate('ARCHIVE.MONTH_NAMES.' + month)
      .then(function(monthName) {
        sidebar.search.keywords = monthName + ' ' + year;

        if (!sidebar.isLockedOpen) {
          sidebar.open();
        }
      });

    var searchInput = {
      language: $translate.use(),
      year: year,
      month: month
    };

    return search.run(searchInput);
  }
}
