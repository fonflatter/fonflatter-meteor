'use strict';

var app = angular.module('fonflatter');
app.config(configureTheme);

configureTheme.$inject = ['$mdThemingProvider'];

function configureTheme($mdThemingProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('grey', {
      default: '50'
    })
    .warnPalette('red')
    .accentPalette('blue')
    .backgroundPalette('grey', {
      default: '900'
    });

  $mdThemingProvider.theme('light')
    .backgroundPalette('grey', {
      default: '50'
    });
}
