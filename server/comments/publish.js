Meteor.publish('comments', function (pageId) {
  if (!pageId) {
    return [];
  }

  var comments = Comments.find({
    pageId: pageId
  }, {
    sort: ['date']
  });

  return comments;
});
