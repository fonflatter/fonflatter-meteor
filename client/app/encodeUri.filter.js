'use strict';

var app = angular.module('fonflatter');
app.filter('encodeUri', function() {
  return window.encodeURIComponent;
});
