Meteor.publish('transcriptions', function (pageId) {
  if (!pageId) {
    return [];
  }

  var transcriptions = Transcriptions.find({
    pageId: pageId
  }, {
    sort: ['date']
  });

  return transcriptions;
});
