Transcriptions = new Mongo.Collection('transcriptions', {
  idGeneration: 'MONGO',
  transform: function(transcription) {
    transcription.text = transcription.text.replace(/(?:\r\n|\r|\n)/g, '<br>');

    return transcription;
  }
});

if (Meteor.isServer) {
  Transcriptions._ensureIndex({
    pageId: 'hashed'
  });

  var hasOldMongoDB = (process.env.ROOT_URL === 'http://fonflatter.meteor.com');

  if (hasOldMongoDB) {
    console.log('Running on meteor.com => disabling MongoDB 2.6 features');
  } else {
    Transcriptions._ensureIndex({
      text: 'text'
    });
  }
}
