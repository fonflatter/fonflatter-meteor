'use strict';

var app = angular.module('fonflatter');
app.factory('transcriptions', transcriptionsFactory);

transcriptionsFactory.$inject = [
  '$meteor'
];

function transcriptionsFactory($meteor) {

  var transcriptions = {
    load: loadTranscriptions
  };

  return transcriptions;

  function loadTranscriptions(page) {
    return $meteor.subscribe('transcriptions', page._id)
      .then(function(subscriptionHandle) {
        if (transcriptions.subscriptionHandle) {
          transcriptions.subscriptionHandle.stop();
        }

        transcriptions.subscriptionHandle = subscriptionHandle;
        return page;
      });

  }
}
