'use strict';

var app = angular.module('fonflatter');
app.factory('menu', menuFactory);

menuFactory.$inject = [
  '$rootScope',
  '$mdMedia',
  '$mdSidenav'
];

function menuFactory($rootScope, $mdMedia, $mdSidenav) {
  var locals = {};

  locals.buttonClasses = 'md-fab md-primary md-raised';

  $rootScope.$watch(function() {
    return $mdMedia('(min-width: 1200px)');
  }, function(canBeLockedOpen) {
    locals.isLockedOpen = canBeLockedOpen;
  });

  locals.items = [
    {
      icon: 'home',
      tooltip: 'MENU_COMICS',
      url: 'http://neu.fonflatter.de'
    },
    {
      icon: 'calendar',
      tooltip: 'MENU_ARCHIVE',
      url: '/archive'
    },
    {
      icon: 'shopping-cart',
      tooltip: 'MENU_SHOP',
      url: 'http://shop.fonflatter.de'
    },
    {
      icon: 'user',
      tooltip: 'MENU_IMPRINT',
      url: 'http://www.fonflatter.de/kontakt/'
    }
  ];

  locals.open = function() {
    return $mdSidenav('menu').open();
  };

  locals.close = function() {
    return $mdSidenav('menu').close();
  };

  return locals;
}
