'use strict';

var app = angular.module('fonflatter');

app.controller('TranscriptionsController', TranscriptionsController);

TranscriptionsController.$inject = [
  '$scope'
];

function TranscriptionsController($scope) {

  var vm = this;

  vm.items = $scope.$meteorCollection(Transcriptions);
}
