'use strict';

var app = angular.module('fonflatter');

app.config(configureRoutes);

configureRoutes.$inject = ['$locationProvider',
  '$routeProvider'
];

function configureRoutes($locationProvider, $routeProvider) {
  $locationProvider.html5Mode(true);

  $routeProvider
    .otherwise({
      redirectTo: '/404'
    });

}
