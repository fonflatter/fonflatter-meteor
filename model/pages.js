Pages = new Mongo.Collection('pages', {
  idGeneration: 'MONGO',
  transform: function(page) {
    if (page && page.isComic) {
      addComicData(page);
    }

    return page;
  }
});

if (Meteor.isServer) {
  Pages._ensureIndex({
    date: 'hashed'
  });
}

function addComicData(comic) {
  var comicDate = moment(comic.date);

  comic.url = comicDate.format('/YYYY/MM/DD/');
  comic.image = '/comics/' + comic.language
    + comicDate.format('/YYYY[/fred_]YYYY-MM-DD[.png]');

  var publicDir;

  if (process.env.STATIC_FILES) {
    publicDir = process.env.STATIC_FILES;
  } else {
    publicDir = process.env.PWD + '/public';
  }

  var imageExists = fs.existsSync(publicDir + comic.image);

  if (!imageExists) {
    comic.image = comic.image.replace(/png$/, 'jpg');

    imageExists = fs.existsSync(publicDir + comic.image);
    if (!imageExists) {
      comic.image = '/white.png';
    }
  }
}
