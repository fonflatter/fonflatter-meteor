Meteor.methods({
  addComment: addComment
});

var reader = new commonmark.Parser();
var writer = new commonmark.HtmlRenderer({safe: true});
writer.softbreak = "<br />";

function parseMarkdown(markdown) {
  var parsed = reader.parse(markdown);
  return writer.render(parsed);
}

function addComment(pageId, comment) {
  if (Pages.find(pageId).count() !== 1) {
    throw new Error('Page does not exist!');
  }

  check(comment.author, String);
  check(comment.text, String);

  if (comment.url) {
    check(comment.url, String);
  }

  Comments.insert({
    pageId: pageId,
    date: moment.utc().toDate(),
    author: comment.author,
    url: comment.url,
    text: parseMarkdown(comment.text)
  });
}
