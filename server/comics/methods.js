Meteor.methods({
  findComic: findComic,
  findMostRecentComic: findMostRecentComic,
  loadSearchResults: loadSearchResults,
  search: search
});

var searchResultPartitionSize = 20;

function findComic(language, comicDate) {
  check(language, String);
  check(comicDate, Date);

  var comic = Pages.findOne(
    {
      isComic: true,
      date: comicDate,
      language: language
    }
  );

  if (comic) {
    comic.translations = findComicTranslations(comic);
    comic.prev = findPreviousComic(comic);
    comic.next = findNextComic(comic);
  }

  return comic;
}

function findComicTranslations(comic) {
  var translations = Pages.find({
    isComic: true,
    language: {
      $ne: comic.language
    },
    date: comic.date
  }).fetch();

  _.forEach(translations, function(translation) {
    var hostname = _.findKey(hosts, function(host) {
      return host.language === translation.language;
    });

    translation.url = '//' + hostname + translation.url;
  });

  return translations;
}

function findMostRecentComic(language) {
  check(language, String);

  var comic = Pages.findOne(
    {
      isComic: true,
      date: {$lte: moment().toDate()},
      language: language
    },
    {
      sort: [['date',
        'desc'
      ]
      ]
    }
  );


  if (comic) {
    comic.translations = findComicTranslations(comic);
    comic.prev = findPreviousComic(comic);
  }

  return comic;
}

function findNextComic(comic) {
  return Pages.findOne(
    {
      isComic: true,
      date: {$gt: comic.date},
      language: comic.language
    },
    {
      sort: [['date',
        'asc'
      ]
      ]
    }
  );
}

function findPreviousComic(comic) {
  return Pages.findOne(
    {
      isComic: true,
      date: {$lt: comic.date},
      language: comic.language
    },
    {
      sort: [['date',
        'desc'
      ]
      ]
    }
  );
}

function loadSearchResults(partition) {
  if (partition.length > searchResultPartitionSize) {
    throw new Error('Partition is too large!');
  }

  var pages = Pages.find({
    _id: {
      $in: partition
    }
  });

  return pages.fetch();
}

function search(input) {
  if (input.keywords) {
    return searchByKeywords(input.keywords);
  } else {
    return searchInArchive(input);
  }
}

function searchByKeywords(keywords) {
  var matches = Pages.searchIndex.search(keywords);
  var pageIds = _.map(matches, function(match) {
    return new Mongo.ObjectID(match.ref);
  });

  var result = {
    partitions: [],
    partitionIndex: 0
  };

  var numPartitions = Math.ceil(pageIds.length / searchResultPartitionSize);

  for (var i = 0; i < numPartitions; i++) {
    var partition = pageIds.slice(
      i * searchResultPartitionSize,
      (i + 1) * searchResultPartitionSize
    );
    result.partitions.push(partition);
  }

  if (result.partitions.length > 0) {
    result.pages = loadSearchResults(result.partitions[0]);
  }

  return result;
}

function searchInArchive(input) {
  var minDate = moment.utc({
    year: input.year,
    month: input.month,
    day: 1
  });

  // TODO: maxDate should not be > now, unless admin access
  var maxDate = minDate.clone().add(1, 'month');

  var pages = Pages.find({
    language: input.language,
    $and: [
      {date: {$gte: minDate.toDate()}},
      {date: {$lt: maxDate.toDate()}}
    ]
  }, {
    sort: [['date',
      'asc'
    ]
    ],
    limit: 31
  });

  return {
    pages: pages.fetch()
  };
}
