'use strict';

var app = angular.module('fonflatter');

app.controller('CommentsController', CommentsController);

CommentsController.$inject = [
  '$scope'
];

function CommentsController($scope) {

  var vm = this;

  vm.items = $scope.$meteorCollection(Comments);
}
