'use strict';

var app = angular.module('fonflatter');

app.config(configureRoutes);

configureRoutes.$inject = ['$routeProvider'];

function configureRoutes($routeProvider) {

  var comicRoute = {
    templateUrl: 'client/comic/comic.ng.html',
    controller: 'ComicController',
    controllerAs: 'comic'
  };

  $routeProvider
    .when('/', comicRoute)
    .when('/:year/:month/:day/', comicRoute);

}
