'use strict';

var app = angular.module('fonflatter');
app.factory('sidebar', sidebarFactory);

sidebarFactory.$inject = [
  '$rootScope',
  '$mdMedia',
  '$mdSidenav',
  'search'
];

function sidebarFactory($rootScope, $mdMedia, $mdSidenav, search) {
  var sidebar = {
    buttonClasses: 'md-fab md-primary md-raised',
    close: close,
    isOpen: isOpen,
    open: open,
    search: search
  };

  $rootScope.$watch(function() {
    return $mdMedia('(min-width: 1100px)');
  }, function(canBeLockedOpen) {
    sidebar.isLockedOpen = canBeLockedOpen;
  });

  return sidebar;

  function close() {
    return $mdSidenav('sidebar').close();
  }

  function isOpen() {
    return $mdSidenav('sidebar').isOpen();
  }

  function open() {
    return $mdSidenav('sidebar').open();
  }
}

